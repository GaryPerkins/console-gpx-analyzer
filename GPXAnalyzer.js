/**
 * Created by Gary P on 4/27/2015.
 *
 * Assistance with the appropriate maths elucidated by Don Cross (cosinekitty.com/compass.html)
 *
 * Run as a command line program with a single GPX file as an argument
 */
if(process.argv.length < 3) {
    console.log("ERROR: Too few arguments.\nProper usage: node GPXAnalyzer.js [xml_filename]\nExit code: 1");
    process.exit(1);
}
else if(process.argv.length > 3) {
    console.log("ERROR: Too many arguments.\nProper usage: node GPXAnalyzer.js [xml_filename]\nExit code: 2");
    process.exit(2);
}

var expat = require('node-expat');
var parser = new expat.Parser('UTF-8');

function TrackObj() {
    this.latitudes = [];
    this.longitudes = [];
    this.elevations = [];
    this.isElevation = false;
    this.isTrkSeg = false;

    this.calculateDistance = function() {
        var distance = 0;
        for(var i = 0; i < this.latitudes.length - 1; i++) {
            var coordsA = this.toCoordinatesObj(this.latitudes[i], this.longitudes[i], this.elevations[i]);
            var coordsB = this.toCoordinatesObj(this.latitudes[i + 1], this.longitudes[i + 1], this.elevations[i + 1]);

            var diffOfXSquared = (coordsA.x - coordsB.x) * (coordsA.x - coordsB.x);
            var diffOfYSquared = (coordsA.y - coordsB.y) * (coordsA.y - coordsB.y);
            var diffOfZSquared = (coordsA.z - coordsA.z) * (coordsA.z - coordsB.z);

            distance += Math.sqrt(diffOfXSquared + diffOfYSquared + diffOfZSquared);
        }
        return distance;
    };
    this.toCoordinatesObj = function(lat, lon, ele) {
        var RADIUS_OF_EARTH_IN_METERS = 6378100;
        var latInRadians = lat * Math.PI / 180;
        var lonInRadians = lon * Math.PI / 180;
        var radius = ele + RADIUS_OF_EARTH_IN_METERS;
        var cosOfLon = Math.cos(lonInRadians);
        var sinOfLon = Math.sin(lonInRadians);
        var cosOfLat = Math.cos(latInRadians);
        var sinOfLat = Math.sin(latInRadians);
        var x = cosOfLon * cosOfLat * radius;
        var y = sinOfLon * cosOfLat * radius;
        var z = sinOfLat * radius;
        return {
            'x': x,
            'y': y,
            'z': z
        };
    };
}

var theTrack = new TrackObj();

parser.on('startElement', function (name, attrs) {
    if(name == "trkpt") {
        theTrack.latitudes.push(Number(attrs['lat']));
        theTrack.longitudes.push(Number(attrs['lon']));
    }
    else if(name == "ele" && theTrack.isTrkSeg) {
        theTrack.isElevation = true;
    }
    else if(name == "trkseg") {
        theTrack.isTrkSeg = true;
    }
});

parser.on('endElement', function (name) {
    if(name == "ele") {
        theTrack.isElevation = false;
    }
    else if(name == "trkseg") {
        theTrack.isTrkSeg = false;
    }
});

parser.on('text', function (text) {
    if(theTrack.isElevation) {
        theTrack.elevations.push(Number(text));
    }
});

parser.on('error', function (error) {
    console.error(error);
});

var fs = require('fs');
var fstrm;
var xmlStr = "";
var file = process.argv[2];
fstrm = fs.createReadStream(file);
fstrm.on('data', function(chunk) {
    xmlStr += chunk.toString();
});
fstrm.on('end', function() {
    parser.write(xmlStr);
    // print out distance to user in both meters and kilometers, rounding to nearest hundredths decimal place
    console.log("The distance of the track is " + Math.round(theTrack.calculateDistance() * 100) / 100 + " meters. (" +
        Math.round(theTrack.calculateDistance() *.001 * 100) / 100 + " kilometers)");
});